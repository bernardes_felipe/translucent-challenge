FROM node:8.2.1

RUN set -x \
    && apt-get update \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && mkdir /src \
    && npm install --silent -g gulp \
    && npm install

WORKDIR /src

EXPOSE 3000

CMD npm start
