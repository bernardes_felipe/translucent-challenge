(angular => {
  'use strict';

  angular.module('Config', [
    "ngMaterial",
    "ngMdIcons",
    "mdPickers",
    "ngRoute",
  ])
    .config(['$routeProvider', '$locationProvider', 'VIEW_PATH', configRouter])
    .config(['$mdThemingProvider', configColor])
    .config(['$mdIconProvider', configIcons])
    .constant("VIEW_PATH", "/view")
    .run(['GameService', run])

  function run(GameService) {
    if (GameService.list().length > 0) return;

    const seedData = [
      {
        title: 'Crash Bandicoot',
        year: 1996,
        console: 'PS1',
        completed: false,
        dateOfCompletion: null,
        personalNotes: 'dude, this game is awesome! gotta play it again on my vacation 😄'
      },
      {
        title: 'Metal Gear Solid 2',
        year: 2001,
        console: 'PS2',
        completed: true,
        dateOfCompletion: new Date('2010-05-04'),
        personalNotes: 'I cried in the end 😢'
      },
      {
        title: 'Hotline Miami',
        year: 2012,
        console: 'PC',
        completed: true,
        dateOfCompletion: new Date('2017-08-08'),
        personalNotes: 'I need one of those cool masks 🐷 😎'
      }
    ];

    GameService.seed(seedData);
  }

  function configRouter($routeProvider, $locationProvider, VIEW_PATH) {
    $routeProvider
      .when('/', {
        controller: 'App.HomeCtrl',
        controllerAs: '$ctrl',
        templateUrl: `${VIEW_PATH}/`
      })
      .otherwise({ redirectTo: '/' });
  }

  function configColor($mdThemingProvider) {
    $mdThemingProvider.definePalette('customPrimary', {
       '50': '#b9f2b4',
       '100': '#a5ef9e',
       '200': '#91eb88',
       '300': '#7de773',
       '400': '#68e45d',
       '500': '#54e047',
       '600': '#40dc31',
       '700': '#32d123',
       '800': '#2dbb20',
       '900': '#27a51c',
       'A100': '#cef6ca',
       'A200': '#e2fae0',
       'A400': '#f6fdf6',
       'A700': '#228f18',
       'contrastDefaultColor': 'light'
    });

    $mdThemingProvider.definePalette('customAccent', {
        '50': '#0c3734',
        '100': '#114c47',
        '200': '#114c47',
        '300': '#114c47',
        '400': '#114c47',
        '500': '#114c47',
        '600': '#114c47',
        '700': '#114c47',
        '800': '#114c47',
        '900': '#114c47',
        'A100': '#114c47',
        'A200': '#114c47',
        'A400': '#114c47',
        'A700': '#114c47',
        'contrastDefaultColor': 'light'
    });

    $mdThemingProvider.theme('default')
      .primaryPalette('customPrimary')
      .accentPalette('customAccent')
      .warnPalette('red');
  }

  function configIcons($mdIconProvider) {
    $mdIconProvider.defaultIconSet('/svg/mdi.svg');
  }

})(angular);
