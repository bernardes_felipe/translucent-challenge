(angular => {
  "use strict";

  angular.module("App", ["Config"])
  .controller("AppCtrl", ['$rootScope', '$scope', '$mdSidenav', '$mdMedia', AppCtrl]);

  function AppCtrl($rootScope, $scope, $mdSidenav, $mdMedia) {
    this.toggleSidenav = (menuId) => $mdSidenav(menuId).toggle();
    this.hideSidebar = () => $mdMedia('min-width: 768px');
  };

})(angular);
