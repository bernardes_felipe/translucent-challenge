(angular => {

  angular.module("App")

  .config(['$routeProvider', 'VIEW_PATH', configRouter]);

  function configRouter($routeProvider, VIEW_PATH) {

    $routeProvider
      .when('/', {
        controller: 'App.HomeCtrl',
        controllerAs: '$ctrl',
        templateUrl: `${VIEW_PATH}/app/home.html`
      })
      .when('/catalog', {
        controller: 'App.CatalogCtrl',
        controllerAs: '$ctrl',
        templateUrl: `${VIEW_PATH}/app/catalog.html`
      });
  }

})(angular);
