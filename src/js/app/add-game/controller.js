(angular => {
  'use strict';

  angular.module('App')
    .controller('App.AddGameCtrl', ['$scope', '$mdDialog', 'Toast', 'GameService', AddGameCtrl]);

  function AddGameCtrl($scope, $mdDialog, Toast, GameService) {
    this.data = {};
    this.consoles = [
      'GameBoy Color',
      'GameBoy Advance',
      'GameCube',
      'Nintendo DS',
      'Nintendo 3DS',
      'Nintendo 64',
      'Nintendo Switch',
      'PC',
      'Playstation 1',
      'Playstation 2',
      'Playstation 3',
      'Playstation 4',
      'PS Vita',
      'Wii',
      'Wii U',
      'Xbox',
      'Xbox 360',
      'Xbox One'
    ];

    this.save = () => {
      if (!this.data.title || !this.data.console || !this.data.year) {
        Toast.show("Please fill the required fields");
        return;
      }
      this.error = "";
      GameService.save(this.data);
      $mdDialog.hide();
      Toast.show("Game added to catalog");
    };

    this.cancel = $mdDialog.cancel;
  }

})(angular);
