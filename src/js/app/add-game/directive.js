(angular => {
  'use strict';

  angular.module('App')
  .directive('addGame', ['$mdDialog', 'VIEW_PATH', AddGame]);

  function AddGame($mdDialog, VIEW_PATH) {
    return {
      restrict: 'A',
      scope: {
        onDone: "&onDone"
      },
      link: (scope, element) => {
        element.on('click', ($event) => {
          $mdDialog.show({
            templateUrl: `${VIEW_PATH}/app/add-game.html`,
            controller: 'App.AddGameCtrl',
            controllerAs: '$ctrl',
            clickOutsideToClose: true,
            escapeToClose: true,
            targetEvent: $event
          }).then(() => {
            const callback = scope.onDone();
            if (callback) {
              callback();
            }
          });
        });
      }
    };
  }
})(angular);
