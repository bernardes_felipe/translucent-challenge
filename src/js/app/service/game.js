(angular => {
  'use strict';

  angular.module('App')

  .factory("GameService", ['$window', GameService]);

  function GameService($window) {

    const seed = (data) => $window.localStorage.setItem('games', JSON.stringify(data));
    const list = () => JSON.parse($window.localStorage['games'] || '[]');
    const save = (game) => {
      const games = list();
      games.push(game);
      $window.localStorage.setItem('games', JSON.stringify(games));
    }

    return {
      list,
      save,
      seed
    };
  }
})(angular);
