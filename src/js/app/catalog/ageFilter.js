(angular => {

  angular.module("App")

  .filter('ageFilter', [ageFilter]);

  function ageFilter() {
    return (year) => {
      if (!year) return '-';

      const diff = moment().diff(new Date(`${year}-01-01`), 'years');

      if (diff === 0) return `published this year`;
      if (diff === 1) return `${diff} year old`;
      return `${diff} years old`;
    };
  }

})(angular);
