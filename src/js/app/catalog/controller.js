(angular => {
  'use strict';

  angular.module('App')
    .controller('App.CatalogCtrl', ['GameService', CatalogCtrl]);

  function CatalogCtrl(GameService) {
    this.games = [];

    this.loadData = () => {
      this.games = GameService.list();
    };

    this.loadData();
  }

})(angular);
