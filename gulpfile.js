const gulp = require('gulp'),
      autoprefixer = require('gulp-autoprefixer'),
      babel = require('gulp-babel'),
      browserSync = require('browser-sync').create(),
      concat = require('gulp-concat'),
      pug = require('gulp-pug'),
      rimraf = require('gulp-rimraf'),
      sass = require('gulp-sass'),
      uglify = require('gulp-uglify');

const jsPath = [
  'src/js/**/*.js',
  'src/js/app.js'
];

gulp.task('clean', () =>
  gulp.src(['dist'], {
    read: false
  })
  .pipe(rimraf())
);

gulp.task('libs:js', () =>
  gulp.src([
    'node_modules/angular/angular.js',
    'node_modules/moment/min/moment.min.js',
    'node_modules/angular-route/angular-route.min.js',
    'node_modules/angular-aria/angular-aria.min.js',
    'node_modules/angular-animate/angular-animate.min.js',
    'node_modules/mdPickers/dist/mdPickers.min.js',
    'node_modules/angular-material/angular-material.min.js'
  ])
    .pipe(concat('libs.min.js'))
    .pipe(gulp.dest('dist/js'))
);

gulp.task('libs:css', () =>
  gulp.src([
    'node_modules/animate.css/animate.min.css',
    'node_modules/angular-material/angular-material.min.css',
    'node_modules/mdPickers/dist/mdPickers.min.css',
    'node_modules/angular-material-icons/angular-material-icons.css',
    'node_modules/material-design-icons/iconfont/material-icons.css'
  ])
    .pipe(concat('libs.css'))
    .pipe(gulp.dest('dist/css'))
);

gulp.task('js', () =>
  gulp.src(jsPath)
    .pipe(concat('all.min.js'))
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
);

gulp.task('public', () =>
  gulp.src('src/public/**/*.*')
    .pipe(gulp.dest('dist'))
);

gulp.task('pug', () =>
  gulp.src('src/**/*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('dist'))
);

gulp.task('sass', () =>
  gulp.src('src/**/*.scss')
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(concat('style.css'))
    .pipe(autoprefixer({
      browsers: ['last 20 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('dist/css'))
);

gulp.task('watch', ['compile'], () => {
  gulp.watch(jsPath, ['js']);
  gulp.watch('src/**/*.scss', ['sass']);
  gulp.watch('src/**/*.pug', ['pug']);

  browserSync.init({
    server: {
        baseDir: "dist"
    }
  });
});

gulp.task('libs', ['libs:js', 'libs:css']);
gulp.task('compile', ['public', 'libs', 'js', 'sass', 'pug']);
gulp.task('default', ['watch']);
