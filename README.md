# Translucent Challenge
I do want to work with @ Translucent!

## About The App
MyGames is an web app made to keep track of your  videogames.

## Running

### With docker
Just run `docker-compose up`

*important*: If you're using mac, you might need to run the container `npm run web bash` and install gulp-sass manually `npm install gulp-sass --save-dev`

### With node
Just run `npm install && npm start`
